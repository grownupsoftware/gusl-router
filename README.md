# README #

Router Node Implementation

To use, the recipient node build.gradle should look like the following.

```groovy
buildscript {
    configurations.classpath.resolutionStrategy.cacheChangingModulesFor 0, 'seconds'
    repositories {
        mavenLocal()
        mavenCentral()
        google()

        maven {
            url "https://europe-west2-maven.pkg.dev/gusl-common/gusl-common"
        }
    }

    dependencies {
        classpath "co.gusl:gusl-launcher:${guslVersion}"
        classpath "co.gusl:gusl-gunder:${guslVersion}"
    }
}

apply plugin: 'war'
apply from: '../gusl-build.gradle'
apply plugin: 'gusl.gunder'

dependencies {
    implementation "co.gusl:gusl-core:${guslVersion}"
    implementation "co.gusl:gusl-model:${guslVersion}"
    api "co.gusl:gusl-router:${guslVersion}"
}

war {

    with copySpec {
        (configurations.runtimeClasspath).collect {
            if (it.toString().indexOf("gusl-node") > 0) {
                from(zipTree(it)) {
                    include 'WEB-APP/**'
                    includeEmptyDirs = false
                    eachFile { fcd ->
                        fcd.relativePath = new RelativePath(true, fcd.relativePath.segments.drop(1))
                    }
                }
            }
        }
    }

    with copySpec {
        setDuplicatesStrategy(DuplicatesStrategy.INCLUDE)
        (configurations.runtimeClasspath).collect {
            if (it.toString().indexOf("gusl-router") > 0) {
                from(zipTree(it)) {
                    include 'WEB-APP/**'
                    includeEmptyDirs = false
                    eachFile { fcd ->
                        fcd.relativePath = new RelativePath(true, fcd.relativePath.segments.drop(1))
                    }
                }
            }
        }
    }

    manifest {
        attributes("Application": "gusl.router.application.AbstractRouterApplication")
        attributes("WebSocketSupport": "true")
        attributes("WebSocketPackage": "gusl,smarttech")
    }

    duplicatesStrategy "exclude"
    rootSpec.exclude('exclude/**')
    rootSpec.exclude("**/undertow-*.jar")
    rootSpec.exclude("**/jboss-servlet-api_*.jar")
    rootSpec.exclude("**/jboss-websocket-api*.jar")
    rootSpec.exclude("**/org.jboss.xnio*.jar")
    rootSpec.exclude("**/xnio*.jar")

    archiveFileName = "router.war"
    archiveBaseName = 'router'
}
```
You will need to implement a RouterApplicationLoader...

```java
@Singleton
public class RouterApplicationLoader extends AbstractRouterApplicationLoader {

    public RouterApplicationLoader() {
    }

}
```

You will also need to have an application.json (and log4j2.xml) in resources.

```json
{
  "node-type": "ROUTER",
  "registra-port": 40905,
  "event-bus-config": {
    "threads": "+0",
    "repository-location": "${ApplicationRepository:repo}"
  },
  "cache-groups": [
    "SYSTEM"
  ],
  "router-client-config": {
    "url": "${ApplicationRouter:http://localhost:9000}",
    "registra-address": "${ApplicationRegistra:localhost:40905}",
    "http-client-config": {
      "io-threads": "+0",
      "max-connections": 12,
      "connect-timeout": 2000,
      "socket-timeout": 600000
    }
  },
  "gateway-access": {
    "account-id": 1,
    "access-token": "2ce3f858-38b8-11e8-b467-0ed5f89f718b"
  }
}

```
