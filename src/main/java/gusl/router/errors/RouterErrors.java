package gusl.router.errors;

import gusl.core.errors.ErrorDO;
import gusl.core.exceptions.GUSLErrorException;

/**
 * @author dhudson
 * @since 31/08/2021
 */
public enum RouterErrors {

    NODE_ROUTE_NODE("GUSLSE01 Router node header missing", "route.node.header.missing"),
    NO_ROUTE_TO_NODE("GUSLSE02 No route to node {0} {1}", "no.route.to.node"),
    IO_EXCEPTION("GUSLSE03 IO Exception {0}", "io.exception"),
    REQUEST_FAILED("GUSLSE04 Proxy request failed", "proxy.request.failed"),
    REQUEST_CANCELLED("GUSLSE05 Proxy request cancelled", "proxy.request.cancelled");

    private final String message;
    private final String messageKey;

    RouterErrors(String message, String messageKey) {
        this.message = message;
        this.messageKey = messageKey;
    }

    public ErrorDO getError() {
        return new ErrorDO(message, messageKey);
    }

    public ErrorDO getError(String... params) {
        if (params != null) {
            return new ErrorDO(null, message, messageKey, params);
        } else {
            return new ErrorDO(message, messageKey);
        }
    }

    public ErrorDO getError(boolean ignoreLogging, String... params) {
        if (params != null) {
            return new ErrorDO(ignoreLogging, null, message, messageKey, params);
        } else {
            return new ErrorDO(ignoreLogging, message, messageKey);
        }
    }

    public GUSLErrorException generateException(String... params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(String params) {
        return new GUSLErrorException(getError(params));
    }

    public GUSLErrorException generateException(boolean ignoreLogging, String params) {
        return new GUSLErrorException(getError(ignoreLogging, params));
    }

    public GUSLErrorException generateException(Throwable t, String... params) {
        return new GUSLErrorException(getError(params), t);
    }

    public GUSLErrorException generateExceptionNoLogging(String param) {
        final ErrorDO error = getError(param);
        error.setIgnoreLogging(true);
        return new GUSLErrorException(getError(param));
    }

    public GUSLErrorException generateExceptionNoLogging(String... params) {
        final ErrorDO error = getError(params);
        error.setIgnoreLogging(true);
        return new GUSLErrorException(error);
    }

    public String getMessage() {
        return message;
    }
}
