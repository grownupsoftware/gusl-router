package gusl.router.publish;

import gusl.model.nodeconfig.NodeType;
import gusl.node.properties.GUSLConfigurable;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 */
@Contract
public interface PublishService extends GUSLConfigurable {

    /**
     * We want to know where the message comes from, so we don't send it back.
     * <p>
     * It is possible to have a fake from node (Testing, Postman etc), which is
     * fine, as they probably will not be registered with the router.
     *
     * @param from  node the request is from
     * @param event request
     */
    public void publish(Integer from, Object event);

    /**
     * This will wait for all the nodes to respond before returning.
     * <p>
     * This in turn will use the EventBus resource, waitingEventBusRequest and
     * that will use postWithFuture.
     * <p>
     * When this method completes, all nodes and processed the events.
     *
     * @param from
     * @param event
     */
    public void publishAndWait(Integer from, Object event);

    /**
     * Only publish the event to the known Node with an ID.
     *
     * @param to
     * @param event
     */
    public void publishOnlyTo(Integer to, Object event);

    /**
     * Broadcast event to all nodes of a type.
     *
     * @param type
     * @param event
     */
    public void publishToAllNodesOfType(NodeType type, Object event);

    /**
     * @param type
     * @param event
     * @param nodeId
     */
    public void publishToAllNodesOfTypeExcept(NodeType type, Object event, int nodeId);

    /**
     * Publish the event only to the master node.
     *
     * @param type
     * @param event
     */
    public void publishToOnlyMasterOfType(NodeType type, Object event);

    /**
     * @param type
     * @param event
     */
    public void publishToOneNodeOfType(NodeType type, Object event);

    /**
     * @param type
     * @param event
     * @param hash
     */
    public void publishToBalancedNodeOfType(NodeType type, Object event, long hash);

    public void processPublishRequest(PublishRequest request);
}
