package gusl.router.publish;

import com.codahale.metrics.Meter;
import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.metrics.MetricsFactory;
import gusl.core.reactive.Reactive;
import gusl.model.cache.AbstractCacheActionEvent;
import gusl.model.cache.CacheGroup;
import gusl.model.metrics.MetricsNames;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.NodeDetails;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.node.eventbus.NodeEventBus;
import gusl.node.events.SystemReadyEvent;
import gusl.router.client.RouterHttpClient;
import gusl.router.config.GatewayAccess;
import gusl.router.config.RouterConfig;
import gusl.router.events.SystemUpEvent;
import gusl.router.model.ApplicationNodeHelper;
import gusl.router.model.HttpNodeRequest;
import gusl.router.register.RegistraService;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.time.Duration;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static gusl.model.cache.CacheType.STATIC;
import static gusl.router.publish.PublishRequestCommands.*;

/**
 * @author dhudson
 */
@Service
@CustomLog
public class PublishServiceImpl implements PublishService {

    @Inject
    private RegistraService theRegistra;

    @Inject
    private NodeEventBus theEventBus;

    @Inject
    private RouterHttpClient theRouterClient;

    @Inject
    private NodeDetails theNodeDetails;

    private GatewayAccess theGatewayAccess;

    private final Meter theRequestsMeter;
    private final Meter theCacheMeter;
    private final Meter theNotSubscribedMeter;
    private final Meter theCacheUpsMeter;

    public PublishServiceImpl() {
        theRequestsMeter = MetricsFactory.getMeter(MetricsNames.ROUTER_PUBLISH_REQUESTS);
        theCacheMeter = MetricsFactory.getMeter(MetricsNames.ROUTER_CACHE_UPDATE_EVENTS);
        theNotSubscribedMeter = MetricsFactory.getMeter(MetricsNames.ROUTER_CACHE_NOT_SUBSCRIBED_EVENTS);
        theCacheUpsMeter = MetricsFactory.getMeter(MetricsNames.ROUTER_CACHE_UPDATE_SENT);
    }

    private void cacheUpdate(Integer from, Object event) {
        theCacheMeter.mark();

        AbstractCacheActionEvent<?> cacheEvent = (AbstractCacheActionEvent<?>) event;
        // Regardless of group, you are all getting it
        if (cacheEvent.getCacheType() == STATIC) {
            publishStandardEvent(from, event);
            return;
        }

        CacheGroup group = cacheEvent.getCacheGroup();

        if (from != -1) {
            if (theNodeDetails.getCacheGroups().contains(group)) {
                // Let fire the event internally.
                theEventBus.post(event);
            }
        }

        final List<ResourceNode> targets = new ArrayList<>(5);

        for (ResourceNode target : theRegistra.getActiveNodes()) {
            if (target.getNodeId() == from || target.getNodeId() == theNodeDetails.getNodeId()) {
                // Don't send the event back to yourself
                continue;
            }

            if (isSubscribed(target, group)) {
                targets.add(target);
                theCacheUpsMeter.mark();
            } else {
                theNotSubscribedMeter.mark();
            }
        }

        publishEventTo(targets, event);
    }

    private boolean isSubscribed(ResourceNode target, CacheGroup group) {
        if (target.getCacheGroups() == null || target.getCacheGroups().isEmpty()) {
            return false;
        }

        return target.getCacheGroups().contains(group);
    }

    @Override
    public void publish(Integer from, Object event) {
        theRequestsMeter.mark();
        if (event instanceof AbstractCacheActionEvent) {
            cacheUpdate(from, event);
        } else {
            publishStandardEvent(from, event);
        }
    }

    private void publishStandardEvent(Integer from, Object event) {
        if (from != -1) {
            // Let fire the event internally.
            theEventBus.post(event);
        }

        final List<ResourceNode> targets = new ArrayList<>(4);

        for (ResourceNode target : theRegistra.getActiveNodes()) {
            if (target.getNodeId() == from) {
                // Don't send the event back to yourself
                continue;
            }

            if (target.getNodeId() == theNodeDetails.getNodeId()) {
                // the event bus post has done this
                continue;
            }

            targets.add(target);
        }

        publishEventTo(targets, event);
    }

    private void publishEventTo(final List<ResourceNode> nodes, final Object event) {
        for (ResourceNode target : nodes) {
            HttpNodeRequest<Void> request = new HttpNodeRequest<>();
            request.setPath("/eventbus");
            request.setPayload(event);
            request.setUrl(ApplicationNodeHelper.getURL(target, "/eventbus"));
            theRouterClient.sendRequest(request);
        }
    }

    @Override
    public void publishAndWait(Integer from, Object event) {

        // Let fire the event internally.
        Reactive reactive = Reactive.from(theEventBus.postWithFuture(event));

        for (ResourceNode target : theRegistra.getActiveNodes()) {
            if (target.getNodeId() == from) {
                // Don't send the event back to yourself
                continue;
            }

            if (target.getNodeId() == theNodeDetails.getNodeId()) {
                // the event bus post has done this
                continue;
            }

            HttpNodeRequest<Void> request = new HttpNodeRequest<>();
            request.setPath("/eventbus/wait");
            request.setPayload(event);
            request.setUrl(ApplicationNodeHelper.getURL(target, "/eventbus/wait"));
            reactive.addFuture(theRouterClient.sendRequest(request));
//            logger.debug("Sending publish wait event {} to {}", event.getClass().getName(), path);
//
//            Invocation.Builder builder = theRouterClient.getHttpClient().getJsonBuilder(path);
//            logger.debug("Sending publish event {} to {}", event.getClass().getName(), path);
//            reactive.addFuture(rbuilder.async().post(Entity.json(event), new LoggedFutureCallback(logger, path)));

        }

        reactive.timeout(Duration.ofMillis(10000));
        reactive.onError(throwable -> {
            logger.warn("Timeout waiting for nodes to respond");
        });
        reactive.waitForResult();
    }

    private String generateRequest(ResourceNode target, String endpoint) {
        return target.getApplicationUrl() + endpoint;
    }

    @Override
    public void configure(NodeConfig config) throws GUSLErrorException {
        theGatewayAccess = ((RouterConfig) config).getGatewayAccess();
    }

    // System Up is internal, SystemReady is external
    @OnEvent
    public void handleSystemUpEvent(SystemUpEvent event) {
        SystemReadyEvent publishEvent = new SystemReadyEvent();
        publishEvent.setTimestamp(new Date());
        publish(-1, publishEvent);
    }

    @Override
    public void publishOnlyTo(final Integer to, final Object event) {
        List<ResourceNode> target = new ArrayList<>(1);
        target.add(theRegistra.getNodeById(to));
        publishEventTo(target, event);
    }

    @Override
    public void publishToAllNodesOfType(final NodeType type, final Object event) {
        final List<ResourceNode> targets = new ArrayList<>(theRegistra.getActiveNodesByType(type));
        publishEventTo(targets, event);
    }

    @Override
    public void publishToOnlyMasterOfType(final NodeType type, final Object event) {
        final List<ResourceNode> target = new ArrayList<>(1);
        for (ResourceNode node : theRegistra.getActiveNodesByType(type)) {
            if (node.getMaster()) {
                target.add(node);
                publishEventTo(target, event);
                return;
            }
        }
    }

    @Override
    public void publishToOneNodeOfType(final NodeType type, final Object event) {
        publishToBalancedNodeOfType(type, event, 0);
    }

    @Override
    public void publishToBalancedNodeOfType(final NodeType type, final Object event, final long hash) {
        final List<ResourceNode> target = new ArrayList<>(1);
        ResourceNode node = theRegistra.getRoutedNodeFor(type, hash);
        if (node != null) {
            target.add(node);
            publishEventTo(target, event);
        }
    }

    @Override
    public void publishToAllNodesOfTypeExcept(final NodeType type, final Object event, final int nodeId) {
        List<ResourceNode> targets = new ArrayList<>(3);
        for (ResourceNode node : theRegistra.getActiveNodesByType(type)) {
            if (node.getNodeId() != nodeId) {
                targets.add(node);
            }
        }
        publishEventTo(targets, event);
    }

    private void publishToAllGivenNodes(Object event, List<NodeType> types) {
        List<ResourceNode> targets = new ArrayList<>(3);
        for (ResourceNode node : theRegistra.getActiveNodes()) {
            if (types.contains(node.getType())) {
                targets.add(node);
            }
        }

        publishEventTo(targets, event);
    }

    private void publishToAllNodesExcept(Object event, List<NodeType> types) {
        List<ResourceNode> targets = new ArrayList<>(3);
        for (ResourceNode node : theRegistra.getActiveNodes()) {
            if (!types.contains(node.getType())) {
                targets.add(node);
            }
        }

        publishEventTo(targets, event);
    }

    @Override
    public void processPublishRequest(PublishRequest request) {

        switch (request.getOperation()) {
            case PUBLISH_ONLY_TO:
                publishOnlyTo(request.getToNode(), request.getEvent());
                break;
            case PUBLISH_ALL_NODES_OF_TYPE:
                publishToAllNodesOfType(request.getType(), request.getEvent());
                break;
            case PUBLISH_ALL_NODES_OF_TYPE_EXCEPT:
                publishToAllNodesOfTypeExcept(request.getType(), request.getEvent(), request.getExclude());
                break;
            case PUBLISH_TO_MASTER_NODE:
                publishToOnlyMasterOfType(request.getType(), request.getEvent());
                break;
            case PUBLISH_TO_ONE_NODE_OF_TYPE:
                publishToOneNodeOfType(request.getType(), request.getEvent());
                break;
            case PUBLISH_TO_BALANCED_NODE_OF_TYPE:
                publishToBalancedNodeOfType(request.getType(), request.getEvent(), request.getHash());
                break;
            case PUBLISH_TO_ALL_GIVEN_NODES:
                publishToAllGivenNodes(request.getEvent(), request.getTypes());
                break;
            case PUBLISH_TO_NODES_EXCEPT_GIVEN:
                publishToAllNodesExcept(request.getEvent(), request.getTypes());
                break;
        }
    }

}
