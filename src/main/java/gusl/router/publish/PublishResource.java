package gusl.router.publish;

import gusl.core.utils.ClassUtils;
import gusl.model.nodeconfig.NodeDetails;
import gusl.node.resources.AbstractBaseResource;
import gusl.node.transport.HttpUtils;
import lombok.CustomLog;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import java.lang.reflect.InvocationTargetException;
import java.net.UnknownHostException;

/**
 * @author dhudson
 */
@Singleton
@Path("/publish")
@CustomLog
public class PublishResource extends AbstractBaseResource {

    @Inject
    private PublishService thePublishService;

    @Inject
    private NodeDetails theNodeDetails;

    @POST
    /**
     * Broadcast an event to all nodes, (including this one).
     *
     */
    public void publish(@Suspended final AsyncResponse asyncResponse, @Context final HttpServletRequest servletRequest) {
        try {
            // ID of the node that sent the request
            Integer nodeId = servletRequest.getIntHeader(HttpUtils.NODE_PUBLISH_NODE_ID);
            if (nodeId != null) {
                Object event = HttpUtils.getPayloadFromHttpRequest(servletRequest, asyncResponse);
                if (event != null) {
                    thePublishService.publish(nodeId, event);
                }
            } else {
                logger.warn("Node ID missing from publish request {}", servletRequest.getHeader(HttpUtils.BODY_CONTENT_CLASS_HEADER_NAME));
            }
        } catch (Throwable t) {
            logger.warn("Exception caught in publish resource {}",
                    servletRequest.getHeader(HttpUtils.BODY_CONTENT_CLASS_HEADER_NAME), t);
        } finally {
            // If the event can't be read, the AsyncResult will be populated
            HttpUtils.resumeAsyncOK(asyncResponse);
        }

    }

    @POST
    @Path("/event")
    /**
     * Take a request from HTTP and create a publish event from it.
     *
     * Note the query string has to contain the class (qualified with package)
     * of the event.
     *
     * The event needs to have an empty constructor.
     */
    public Response publishEvent(@QueryParam("class") String clazz) {
        try {
            Object event = ClassUtils.getNewInstance(Class.forName(clazz));
            thePublishService.publish(theNodeDetails.getNodeId(), event);

        } catch (ClassNotFoundException | IllegalAccessException | InstantiationException |
                 InvocationTargetException ex) {
            logger.warn("Unable to process publish event for class {}", clazz, ex);
        }
        return HttpUtils.getOKResponse();
    }

    @POST
    @Path("/wait")
    public void publishAndWait(@Suspended final AsyncResponse asyncResponse, @Context final HttpServletRequest servletRequest) throws UnknownHostException {

        // ID of the node that sent the request
        Integer nodeId = servletRequest.getIntHeader(HttpUtils.NODE_PUBLISH_NODE_ID);

        // If the event can't be read, the AsyncResult will be populated
        Object event = HttpUtils.getPayloadFromHttpRequest(servletRequest, asyncResponse);
        if (event != null) {
            thePublishService.publishAndWait(nodeId, event);
        }
        HttpUtils.resumeAsyncOK(asyncResponse);
    }

    @POST
    @Path("/command")
    public void publishCommand(@Suspended final AsyncResponse asyncResponse, PublishRequest request) {
        thePublishService.processPublishRequest(request);
        HttpUtils.resumeAsyncOK(asyncResponse);
    }
}
