package gusl.router.monitor;

import gusl.model.nodeconfig.NodeMetrics;

/**
 *
 * @author dhudson
 */
public class MonitorMessage {

    private int nodeId;
    private NodeMetrics metrics;

    public MonitorMessage() {
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

    public NodeMetrics getMetrics() {
        return metrics;
    }

    public void setMetrics(NodeMetrics metrics) {
        this.metrics = metrics;
    }

}
