package gusl.router.monitor;

import gusl.core.exceptions.GUSLException;
import gusl.core.executors.BackgroundThreadPoolExecutor;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.NodeMetrics;
import gusl.node.eventbus.NodeEventBus;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.concurrent.TimeUnit;

/**
 * @author dhudson
 */
@Service
@CustomLog
public class ApplicationMonitorImpl implements ApplicationMonitor {

    @Inject
    private NodeEventBus theEventBus;

    public ApplicationMonitorImpl() {
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        BackgroundThreadPoolExecutor.scheduleAtFixedRate(new Runnable() {
            @Override
            public void run() {
                MonitorMessage message = new MonitorMessage();
                message.setNodeId(1);
                message.setMetrics(NodeMetrics.populateFromPlatform());

                theEventBus.post(message);
            }

        }, 1, 1, TimeUnit.SECONDS);
    }

}
