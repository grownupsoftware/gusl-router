package gusl.router.monitor;

import gusl.node.properties.GUSLConfigurable;
import org.jvnet.hk2.annotations.Contract;

/**
 * @author dhudson
 */
@Contract
public interface ApplicationMonitor extends GUSLConfigurable {

}
