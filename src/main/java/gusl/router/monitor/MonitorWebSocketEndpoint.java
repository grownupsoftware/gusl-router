package gusl.router.monitor;

import gusl.core.eventbus.OnEvent;
import gusl.node.bootstrap.GUSLServiceLocator;
import gusl.node.eventbus.NodeEventBus;
import gusl.node.websockets.AbstractRequestAwareWebsocket;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * Client Websocket Endpoint.
 *
 * @author dhudson
 */
@ServerEndpoint(value = "/wsmonitor")
public class MonitorWebSocketEndpoint extends AbstractRequestAwareWebsocket {

    private final NodeEventBus theEventBus;

    public MonitorWebSocketEndpoint() {
        theEventBus = GUSLServiceLocator.getService(NodeEventBus.class);
    }

    @Override
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        super.onOpen(session, config);
        theEventBus.register(this);
    }

    @Override
    @OnClose
    public void onClose(Session session, CloseReason reason) {
        super.onClose(session, reason);
        theEventBus.unregister(this);
    }

    @Override
    @OnError
    public void onError(Session session, Throwable t) {
        super.onError(session, t);
        theEventBus.unregister(this);
    }

    @OnEvent
    public void handleMonitorMessage(MonitorMessage message) {
        writeJSON(message);
    }
}
