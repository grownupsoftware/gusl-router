package gusl.router.application;

import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.NodeType;
import gusl.node.application.AbstractNodeApplication;
import gusl.router.config.RouterConfig;
import org.glassfish.hk2.api.ServiceLocator;

import javax.inject.Inject;
import javax.servlet.ServletContext;

/**
 * The Router Node, (Probably now miss-named) handles routing requests and acts
 * as the registra for Casanova Nodes.
 *
 * @author dhudson
 */
public class RouterApplication extends AbstractNodeApplication {

    @Inject
    public RouterApplication(ServiceLocator serviceLocator, ServletContext context) {
        super(serviceLocator, context);
    }

    @Override
    public NodeType getNodeType() {
        return NodeType.ROUTER;
    }

    @Override
    public Class<? extends NodeConfig> getNodeConfigClass() {
        return RouterConfig.class;
    }

}
