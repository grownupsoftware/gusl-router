package gusl.router.application;

import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLErrorException;
import gusl.core.exceptions.GUSLException;
import gusl.core.utils.Platform;
import gusl.node.application.AbstractApplicationLoader;
import gusl.node.application.events.ContainerStartedEvent;
import gusl.router.server.RouterSocketServer;
import lombok.CustomLog;

import javax.inject.Inject;


/**
 * @author dhudson
 */
@CustomLog
public abstract class AbstractRouterApplicationLoader extends AbstractApplicationLoader {

    @Inject
    private RouterSocketServer theRouterServer;

    public AbstractRouterApplicationLoader() {
        super();
    }


    // Let's do this before the client tries to connect
    @OnEvent(order = 30, failFast = true)
    public void handleContainerStartup(ContainerStartedEvent event) throws GUSLException {
        try {
            theRouterServer.startup();
        } catch (GUSLException ex) {
            logger.warn("Can't start Router Server ", ex);
            throw ex;
        }
    }

    @Override
    public void loadCaches() throws GUSLErrorException {
        // No caches to load, but we will listen for players
    }

    @Override
    public StringBuilder getDisplayBanner() {
        StringBuilder builder = new StringBuilder(1000);
        builder.append(Platform.LINE_SEPARATOR);
        builder.append("__________               __    ");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append("\\______   \\ ____  __ ___/  |_  ___________ ");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(" |       _//  _ \\|  |  \\   __\\/ __ \\_  __ \\");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(" |    |   (  <_> )  |  /|  | \\  ___/|  | \\/");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append(" |____|_  /\\____/|____/ |__|  \\___  >__|   ");
        builder.append(Platform.LINE_SEPARATOR);
        builder.append("        \\/                        \\/    ");
        builder.append(Platform.LINE_SEPARATOR);

        return builder;
    }
}
