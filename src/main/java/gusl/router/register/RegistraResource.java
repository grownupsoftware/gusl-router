package gusl.router.register;

import gusl.core.utils.StringUtils;
import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.node.providers.NoCompress;
import gusl.node.resources.AbstractBaseResource;
import gusl.node.transport.HttpUtils;
import gusl.router.dto.NodesDTO;
import gusl.router.server.RouterSocketServer;
import lombok.CustomLog;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

/**
 * @author dhudson
 */
@Singleton
@Path("/registra")
@CustomLog
public class RegistraResource extends AbstractBaseResource {

    @Inject
    private RegistraService theRegistraService;

    @Inject
    private RouterSocketServer theRouterSocketServer;

    @POST
    @Path("/deregister/{id}")
    public Response deregisterNode(@PathParam("id") int nodeId) {
        logger.info("Deregister Node ...... {} ", nodeId);
        theRegistraService.deregister(nodeId);
        return HttpUtils.getOKResponse();
    }

    @POST
    @Path("/stop/{id}")
    public Response stopNode(@PathParam("id") int nodeId) {
        logger.info("Stop Node {} issued from HTTP Post", nodeId);
        theRegistraService.stopNode(nodeId);
        return HttpUtils.getOKResponse();
    }

    @POST
    @Path("/active")
    public NodesDTO getActiveNodes() {
        NodesDTO response = new NodesDTO();
        response.setNodes(theRegistraService.getActiveNodes());
        return response;
    }

    @POST
    @Path("/quiesce/{id}")
    public Response quiesceNode(@PathParam("id") int nodeId) {
        logger.info("Quiesce Node {} issued from HTTP Post", nodeId);
        ResourceNode node = theRegistraService.getNodeById(nodeId);
        if (node != null) {
            if (node.getStatus() == NodeStatus.ACTIVE) {
                theRouterSocketServer.quiesceNode(nodeId, true);
            }

            if (node.getStatus() == NodeStatus.QUIESCED) {
                theRouterSocketServer.quiesceNode(nodeId, false);
            }
        }
        return HttpUtils.getOKResponse();
    }

    @POST
    @Path("/quiesceAll")
    public Response quiesceAll() {
        logger.info("Quiesce all issued from HTTP Post");
        theRouterSocketServer.quiesceAll(theRegistraService.toggleQuiesceMode());
        return HttpUtils.getOKResponse();
    }

    @NoCompress
    @GET
    @Path("/query")
    public NodesDTO query(@QueryParam("type") String type, @QueryParam("status") String status) {
        logger.info("Query {}, {}", type, status);
        NodeType nodeType = null;
        NodeStatus nodeStatus = null;
        if (StringUtils.isNotBlank(type)) {
            nodeType = NodeType.valueOf(type.toUpperCase());
        }

        if (StringUtils.isNotBlank(status)) {
            nodeStatus = NodeStatus.valueOf(status.toUpperCase());
        }

        NodesDTO response = new NodesDTO();
        List<ResourceNode> results = new ArrayList<>();
        for (ResourceNode node : theRegistraService.getNodes()) {
            if (type != null) {
                if (node.getType() != nodeType) {
                    continue;
                }
            }

            if (status != null) {
                if (node.getStatus() != nodeStatus) {
                    continue;
                }
            }
            results.add(node);
        }

        response.setNodes(results);
        return response;
    }
}
