package gusl.router.register;

import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.router.dto.RegisterRequestDTO;
import gusl.router.dto.RegisterResponseDTO;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

/**
 *
 * @author dhudson
 */
@Contract
public interface RegistraService {

    /**
     * Register a node.
     *
     * Add the node to the registra, with node will not become available until
     * its status is set to active.
     *
     * @param remoteIP
     * @param dto
     * @return
     */
    public RegisterResponseDTO register(String remoteIP, RegisterRequestDTO dto);

    /**
     * Return a list of nodes of a given type.
     *
     * This may include Crashed and failed nodes.
     *
     * @param type
     * @return
     */
    public List<ResourceNode> getNodesByType(NodeType type);

    /**
     * Return a list of nodes of a given type.
     *
     * This may include Crashed and failed nodes.
     *
     * @param type
     * @return
     */
    public List<ResourceNode> getActiveNodesByType(NodeType type);

    /**
     * Return a list of all nodes currently registered.
     *
     * @return
     */
    public List<ResourceNode> getNodes();

    /**
     * Return the node with that ID or null.
     *
     * @param id
     * @return
     */
    public ResourceNode getNodeById(int id);

    /**
     * Get a node of type.
     *
     * This will return a node that is available to handle the request for this
     * type.
     *
     * This may well be a load-balanced approach if more that one type of node
     * is available.
     *
     * @param type
     * @param balanceId
     * @return node or null if none of the type available
     */
    public ResourceNode getRoutedNodeFor(NodeType type, long balanceId);

    /**
     * Change a status of a node.
     *
     * @param nodeId
     * @param status
     */
    public void updateNodeStatus(int nodeId, NodeStatus status);

    /**
     * Un-register the node from the registra.
     *
     * @param id
     */
    public void deregister(int id);

    /**
     * Returns a list of nodes that are not crashed, but may be in a failed
     * state.
     *
     * @return a list of nodes
     */
    public List<ResourceNode> getActiveNodes();

    /**
     * Stop the node.
     *
     * Change its state that the node is not longer ACTIVE and info the node
     * that it is to be stopped.
     *
     * @param nodeId
     */
    public void stopNode(int nodeId);
    
    /**
     * Toggle the Quiesced state.
     * 
     * @return the new state 
     */
    public boolean toggleQuiesceMode();
    
}
