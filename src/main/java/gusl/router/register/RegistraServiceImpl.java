package gusl.router.register;

import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.model.status.NodeChangedMessage;
import gusl.model.status.NodeChangedMessageType;
import gusl.node.converter.ModelConverter;
import gusl.node.eventbus.NodeEventBus;
import gusl.router.dto.RegisterRequestDTO;
import gusl.router.dto.RegisterResponseDTO;
import gusl.router.events.*;
import gusl.router.model.ApplicationNodeHelper;
import gusl.router.route.LoadBalanceFactory;
import gusl.router.route.NodeLoadBalancer;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;

import static gusl.model.nodeconfig.NodeStatus.*;
import static gusl.model.routerconfig.ResourceNode.IDSORT;

/**
 * Registra Service.
 * <p>
 * Keeps track of all nodes in the Casanova Construct.
 *
 * @author dhudson
 */
@Service
@CustomLog
public class RegistraServiceImpl implements RegistraService {

    private final List<ResourceNode> theNodes;
    private final HashMap<NodeType, NodeLoadBalancer> theLoadBalancers;

    @Inject
    private NodeEventBus theEventBus;

    @Inject
    private ModelConverter theModelConverter;

    private boolean systemQuiesced = false;

    public RegistraServiceImpl() {
        theNodes = new CopyOnWriteArrayList<>();
        theLoadBalancers = new HashMap<>();
    }

    /**
     * There is a gap between registration and when the node is available
     * (Undertow) started.
     *
     * @param remoteIP
     * @param dto
     * @return
     */
    @Override
    public synchronized RegisterResponseDTO register(String remoteIP, RegisterRequestDTO dto) {

        try {
            RegisterResponseDTO response = new RegisterResponseDTO();

            int nodeId = getNextAvailableNodeId();
            response.setNodeId(nodeId);

            ResourceNode node = new ResourceNode(remoteIP, dto.getPort(), dto.getType(), nodeId);
            node.setStatus(NodeStatus.STARTING);
            node.setMaster(isMaster(dto.getType()));
            node.setVersion(dto.getVersion());
            node.setCacheGroups(dto.getCacheGroups());

            response.setMaster(node.getMaster());

            if (!theNodes.add(node)) {
                logger.warn("Registering a node that is already there .. {}", node);
            } else {
                logger.info("Registered node {}", node);
            }

            // Notify listeners
            theEventBus.post(new NodeChangedMessage(NodeChangedMessageType.REGISTER, node));


            return response;
        } finally {
            theEventBus.post(new NodeRegisteredEvent(getNodeSnapshot(theNodes)));
        }
    }

    private List<ResourceNode> getNodeSnapshot(List<ResourceNode> current) {
        ArrayList<ResourceNode> list = new ArrayList<>(current.size());
        for (ResourceNode node : current) {
            // Clone
            list.add(theModelConverter.convert(node, ResourceNode.class));
        }
        return list;
    }

    // Check to see if other nodes are already master
    private boolean isMaster(NodeType type) {
        List<ResourceNode> currentNodes = getNodesByType(type);
        if (currentNodes.isEmpty()) {
            // first one of this type so master you are
            return true;
        }

        for (ResourceNode node : currentNodes) {
            if (node.getStatus() == STARTING || node.getStatus() == ACTIVE && node.getMaster()) {
                // We have a master
                return false;
            }
        }

        return true;
    }

    @Override
    public List<ResourceNode> getNodes() {
        return theNodes;
    }

    @Override
    public List<ResourceNode> getNodesByType(NodeType type) {
        List<ResourceNode> nodes = new ArrayList<>(3);
        for (ResourceNode node : theNodes) {
            if (node.getType() == type) {
                nodes.add(node);
            }
        }

        return nodes;
    }

    @Override
    public List<ResourceNode> getActiveNodesByType(NodeType type) {
        List<ResourceNode> nodes = new ArrayList<>(3);
        for (ResourceNode node : theNodes) {
            if (node.getType() == type && node.getStatus() == ACTIVE) {
                nodes.add(node);
            }
        }

        return nodes;
    }

    @Override
    public ResourceNode getRoutedNodeFor(NodeType type, long id) {
        NodeLoadBalancer balancer = theLoadBalancers.get(type);

        if (balancer == null) {
            return null;
        }

        return balancer.getNodeFor(id);
    }

    private int getNextAvailableNodeId() {
        List<ResourceNode> nodes = getNodeSnapshot(theNodes);
        Collections.sort(nodes, IDSORT);

        int id;
        for (int i = 0; i < nodes.size(); i++) {
            id = i + 1;
            if (nodes.get(i).getNodeId() != id) {
                return id;
            }
        }

        return nodes.size() + 1;
    }

    @Override
    public synchronized void updateNodeStatus(int nodeId, NodeStatus status) {
        ResourceNode node = getNodeById(nodeId);
        if (node != null) {
            try {
                NodeType type = node.getType();
                logger.info("Changed status for {}:{} to {}", type, node.getNodeId(), status);

                node.setStatus(status);

                theLoadBalancers.put(type, LoadBalanceFactory.getLoadBalancerFor(type, getActiveNodesByType(type)));

                // Notify the web sockets, if any
                theEventBus.post(new NodeChangedMessage(NodeChangedMessageType.STATUS_CHANGE, node));
            } finally {
                if (status == NodeStatus.ACTIVE) {
                    theEventBus.post(new NodeStartedEvent(getNodeSnapshot(theNodes), node));

                    boolean allActive = true;
                    // Lets make sure that we start a started one of each type
                    Map<NodeType, List<ResourceNode>> theNodesByType = theNodes.stream().filter(casanovaNode -> casanovaNode.getStatus() != NodeStatus.CRASHED).collect(Collectors.groupingBy(ResourceNode::getType));
                    for (List<ResourceNode> nodes : theNodesByType.values()) {
                        for (ResourceNode casanovaNode : nodes) {
                            if (casanovaNode.getStatus() != ACTIVE) {
                                allActive = false;
                            }
                        }
                    }

                    // Tends to fire this, when Router is starting, as its the only node, and they are all active
                    if (theNodes.size() > 1) {
                        if (allActive) {
                            // Lets fire a system ready event
                            SystemUpEvent event = new SystemUpEvent();
                            theEventBus.post(event);
                        }
                    }
                }

                if (status == CRASHED) {
                    theEventBus.post(new NodeCrashedEvent(nodeId));
                }

                theEventBus.post(new NodesChangedEvent(getNodeSnapshot(theNodes)));
            }
        } else {
            logger.warn("There is an update for {}:{}, which is not registered", nodeId, status);
        }
    }

    @Override
    public ResourceNode getNodeById(int id) {
        for (ResourceNode node : theNodes) {
            if (node.getNodeId() == id) {
                return node;
            }
        }

        return null;
    }

    @Override
    public void deregister(int id) {
        ResourceNode node = getNodeById(id);

        if (node == null) {
            logger.warn("Node [" + id + "] not registered");
            return;
        }

        if (node.getStatus() == ACTIVE) {
            logger.warn("Can't deregister an ACTIVE node");
            return;
        }

        theEventBus.post(new NodeChangedMessage(NodeChangedMessageType.DEREGISTER, node));

        theNodes.remove(node);

        logger.info("Node {}:{} deregistered", node.getType(), id);
    }

    @Override
    public List<ResourceNode> getActiveNodes() {
        return ApplicationNodeHelper.getActiveNodes(theNodes);
    }

    @Override
    public void stopNode(int nodeId) {
        ResourceNode node = getNodeById(nodeId);
        if (node != null) {
            updateNodeStatus(nodeId, STOPPING);
            theEventBus.post(new StopNodeEvent(nodeId));
        }
    }

    @Override
    public boolean toggleQuiesceMode() {
        systemQuiesced = !systemQuiesced;
        return systemQuiesced;
    }

}
