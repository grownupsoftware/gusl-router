package gusl.router.server;

import gusl.core.utils.IOUtils;
import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.model.systemconfig.SystemConfigDO;
import gusl.node.bootstrap.GUSLServiceLocator;
import gusl.router.dto.*;
import gusl.router.messaging.*;
import gusl.router.model.ApplicationNodeHelper;
import gusl.router.register.RegistraService;
import lombok.CustomLog;

import java.io.Closeable;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;

/**
 * There is one of these for each node.
 * <p>
 * A socket is used as a fail fast indication that something has gone wrong with
 * the node.
 *
 * @author dhudson
 */
@CustomLog
public class RouterServerChannel implements ConnectionLostHandler, RouterMessageHandler, Closeable {

    private static final List<NodeType> EMPTY_NODE_TYPES = new ArrayList<>(0);

    private final Socket theSocket;
    private boolean isRunning;
    private boolean isClosing;
    private final SocketEnvelopeWriter theWriter;
    private final InboundConsumer theReader;

    private final RegistraService theRegistra;

    // Keep this, as there is one channel per each connection, and this will tell us what it is.
    private RegisterRequestDTO theRegisterRequest;
    private RegisterResponseDTO theResponse;

    private boolean startUpIssued;
    private final Map<String, SystemConfigDO> theSystemConfig;
    private final ExecutorService theServerExecutor;

    public RouterServerChannel(Socket socket, Map<String, SystemConfigDO> configs, ExecutorService routerServerExecutor) throws IOException {
        theSocket = socket;
        theServerExecutor = routerServerExecutor;
        theWriter = new SocketEnvelopeWriter(socket, this);
        theReader = new InboundConsumer(socket, this, this);
        theSystemConfig = configs;
        theRegistra = GUSLServiceLocator.getService(RegistraService.class);
    }

    public void start() {
        isRunning = true;
        theReader.start();
    }

    @Override
    public void close() {
        isRunning = false;
        theReader.shutdown();
        IOUtils.closeQuietly(theSocket);
    }

    @Override
    public void connectionLost(String reason, Throwable cause) {
        close();
        theServerExecutor.execute(() -> {
            if (!isClosing) {
                logger.warn(" Lost connection .... [{}] {}", getNodeId(), reason, cause);
                theRegistra.updateNodeStatus(theResponse.getNodeId(), NodeStatus.CRASHED);
            } else {
                // The client has closed the connection and System.exit
                theRegistra.updateNodeStatus(theResponse.getNodeId(), NodeStatus.STOPPED);
            }
        });
    }

    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public void messageCallback(RouterEnvelope envelope) {
        theServerExecutor.execute(() -> {
            switch (envelope.getMessageType()) {
                case REGISTRATION_REQUEST:

                    theRegisterRequest = (RegisterRequestDTO) envelope.getPayload();

                    theResponse = theRegistra.register(theSocket.getInetAddress().getHostAddress(), theRegisterRequest);
                    // Set the system configs
                    theResponse.setConfigs(theSystemConfig);
                    theWriter.write(RouterMessageType.REGISTRATION_RESPONSE, theResponse);

                    break;

                case STATUS_UPDATE_REQUEST:
                    UpdateStatusDTO update = (UpdateStatusDTO) envelope.getPayload();
                    theRegistra.updateNodeStatus(update.getNodeId(), update.getStatus());
                    break;
            }
        });
    }

    public void issueStartup(List<ResourceNode> nodes) {
        startUpIssued = true;
        theWriter.write(RouterMessageType.STARTUP_REQUEST, createNodeChangeDTO(nodes));
    }

    public void issuesNodeChangeCommand(List<ResourceNode> nodes) {
        if (isRunning) {
            // Only active
            NodeChangeDTO request = new NodeChangeDTO();
            request.setNodes(nodes);
            theWriter.write(RouterMessageType.NODE_CHANGE, request);
        }
    }

    public NodeChangeDTO createNodeChangeDTO(List<ResourceNode> nodes) {
        NodeChangeDTO request = new NodeChangeDTO();
        request.setNodes(ApplicationNodeHelper.getActiveNodes(nodes));
        return request;
    }

    public void sendQuiesceToggle(boolean quiesce) {
        theWriter.write(RouterMessageType.QUIESCE_REQUEST, new QuiesceRequestDTO(quiesce));
    }

    /**
     * Return the node id for this channel, if registered or -1.
     *
     * @return the node id
     */
    public int getNodeId() {
        if (theResponse == null) {
            // Haven't registered yet
            return -1;
        }

        return theResponse.getNodeId();
    }

    public void sendStopCommand() {
        isClosing = true;
        theWriter.write(RouterMessageType.SHUTDOWN_REQUEST);
    }

    public boolean hasIssuedStartup() {
        return startUpIssued;
    }

    public List<NodeType> getRequiredNodes() {
        if (theRegisterRequest == null) {
            return EMPTY_NODE_TYPES;
        }

        if (theRegisterRequest.getRequiredNodes() == null) {
            return EMPTY_NODE_TYPES;
        }

        return theRegisterRequest.getRequiredNodes();
    }

    @Override
    public String toString() {
        if (theRegisterRequest == null || theResponse == null) {
            return super.toString();
        } else {
            return theRegisterRequest.getType() + " : " + theResponse.getNodeId();
        }
    }
}
