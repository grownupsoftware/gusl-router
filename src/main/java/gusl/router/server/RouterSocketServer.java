package gusl.router.server;

import gusl.model.systemconfig.SystemConfigDO;
import gusl.node.properties.GUSLConfigurable;
import gusl.node.service.Controllable;
import org.jvnet.hk2.annotations.Contract;

import java.util.List;

/**
 * @author dhudson
 */
@Contract
public interface RouterSocketServer extends GUSLConfigurable, Controllable {

    public void setSystemConfig(List<SystemConfigDO> config);

    /**
     * Toggle the node.
     *
     * @param nodeId
     * @param quiesce
     */
    public void quiesceNode(int nodeId, boolean quiesce);

    /**
     * Toggle all nodes.
     *
     * @param quiesce
     */
    public void quiesceAll(boolean quiesce);

}
