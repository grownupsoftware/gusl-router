package gusl.router.server;

import gusl.core.eventbus.OnEvent;
import gusl.core.exceptions.GUSLException;
import gusl.core.executors.NamedThreadFactory;
import gusl.core.utils.IOUtils;
import gusl.model.nodeconfig.NodeConfig;
import gusl.model.nodeconfig.NodeStatus;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.model.systemconfig.SystemConfigCacheEvent;
import gusl.model.systemconfig.SystemConfigDO;
import gusl.router.config.RouterConfig;
import gusl.router.events.*;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.function.Function;

import static gusl.core.utils.Utils.safeStream;
import static gusl.router.client.RouterConstants.conditionSocket;
import static java.util.stream.Collectors.toConcurrentMap;

/**
 * @author dhudson
 */
@Service
@CustomLog
public class RouterSocketServerImpl extends Thread implements RouterSocketServer {

    private static final String ROUTER_SERVER_WORKER_THREAD_NAME = "router-server-worker";

    private ServerSocket theServerSocket;
    private volatile boolean isRunning;

    private final List<RouterServerChannel> theChannels;
    private final ExecutorService routerServerExecutor;
    private int theServerPort;

    private Map<String, SystemConfigDO> theSystemConfigMap;

    public RouterSocketServerImpl() {
        super("Router Socket Server");
        routerServerExecutor = Executors.newSingleThreadExecutor(
                new NamedThreadFactory(ROUTER_SERVER_WORKER_THREAD_NAME, true,
                        Thread.NORM_PRIORITY));
        theChannels = new CopyOnWriteArrayList<>();
    }

    @Override
    public void run() {

        logger.info("Waiting for connection(s) on {}", theServerSocket.getLocalPort());

        try {
            while (isRunning) {

                final Socket socket = theServerSocket.accept();
                logger.info("we have a connection on {}", socket.getInetAddress());

                try {
                    // Important that the IO buffers are set the same
                    conditionSocket(socket);

                    RouterServerChannel channel = new RouterServerChannel(socket, theSystemConfigMap, routerServerExecutor);
                    channel.start();

                    // Maintain the list
                    theChannels.add(channel);
                    logger.info("adding {} to channels", channel);
                } catch (IOException ex) {
                    // We can't open this channel
                    logger.warn("Unable to open channel", ex);
                }
            }
        } catch (IOException ex) {
            if (isRunning) {
                logger.warn("Unable to start socket server", ex);
            }
        }

        //thePingProcessor.shutdown();
    }

    @Override
    public void startup() throws GUSLException {
        try {
            theServerSocket = new ServerSocket(theServerPort);
        } catch (IOException ex) {
            throw new GUSLException("Unable to start router server", ex);
        }

        isRunning = true;
        start();
    }

    @Override
    public void shutdown() {
        isRunning = false;
        IOUtils.closeQuietly(theServerSocket);
        IOUtils.closeAllQuietly(theChannels);
    }

    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public void configure(NodeConfig config) throws GUSLException {
        RouterConfig routerConfig = (RouterConfig) config;
        theServerPort = routerConfig.getRegistraPort();
    }

    @OnEvent
    public void handleStopNode(StopNodeEvent event) {
        routerServerExecutor.execute(() -> {
            Iterator<RouterServerChannel> it = theChannels.iterator();
            while (it.hasNext()) {
                RouterServerChannel channel = it.next();
                if (channel.getNodeId() == event.getNodeId()) {
                    // We now have the channel
                    channel.sendStopCommand();
                    theChannels.remove(channel);
                }
            }
        });
    }

    @OnEvent
    public void handleNodeCrashedEvent(NodeCrashedEvent event) {
        routerServerExecutor.execute(() -> {
            Iterator<RouterServerChannel> it = theChannels.iterator();
            while (it.hasNext()) {
                RouterServerChannel channel = it.next();
                if (channel.getNodeId() == event.getNodeId()) {
                    logger.info("Removed node {}", event.getNodeId());
                    theChannels.remove(channel);
                }
            }
        });
    }

    @OnEvent
    public void handleNodeRegisteredEvent(NodeRegisteredEvent event) {
        logger.debug("NodeRegistered event");
        seeIfNodesCanStart(getActiveNodes(event.getNodes()), event.getNodes());
        sendNodeChangedMessage(event.getActiveNodes());
    }

    @OnEvent
    public void handleNodeStartedEvent(NodeStartedEvent event) {
        logger.debug("NodeStarted event");
        routerServerExecutor.execute(() -> {
            seeIfNodesCanStart(getActiveNodes(event.getNodes()), event.getNodes());
            sendNodeChangedMessage(event.getActiveNodes());
        });
    }

    @OnEvent
    public void handleNodeChangedEvent(NodesChangedEvent event) {
        routerServerExecutor.execute(() -> sendNodeChangedMessage(event.getActiveNodes()));
    }

    private void sendNodeChangedMessage(List<ResourceNode> nodes) {
        for (RouterServerChannel channel : theChannels) {
            channel.issuesNodeChangeCommand(nodes);
        }
    }

    private void seeIfNodesCanStart(Set<NodeType> actives, List<ResourceNode> nodes) {

        logger.debug("Actives .. {}", actives);

        // Lets see if we have any channels waiting..
        for (RouterServerChannel channel : theChannels) {
            logger.debug("Channel ...  {}", channel.toString());

            // Not registered yet
            if (channel.getNodeId() != -1) {
                // We are waiting to start
                if (!channel.hasIssuedStartup()) {
                    if (channel.getRequiredNodes().isEmpty()) {
                        logger.debug("Starting {} as it has no dependencies ..", channel);
                        channel.issueStartup(nodes);
                    } else {
                        logger.debug("Seeing if we can start {}", channel);
                        boolean canStart = true;
                        for (NodeType type : channel.getRequiredNodes()) {

                            logger.debug("Required Type {}", type);
                            if (!actives.contains(type)) {
                                logger.debug("Can't start {} as {} not active", channel, type);
                                canStart = false;
                                break;
                            }
                        }

                        if (canStart) {
                            logger.info("Issuing startup {}", channel);
                            channel.issueStartup(nodes);
                        }
                    }
                }
            }
        }
    }

    private Set<NodeType> getActiveNodes(List<ResourceNode> nodes) {
        Set<NodeType> activeSet = new HashSet<>();
        for (ResourceNode node : nodes) {
            if (node.getStatus() == NodeStatus.ACTIVE) {
                activeSet.add(node.getType());
            }
        }

        return activeSet;
    }

    @Override
    public void setSystemConfig(List<SystemConfigDO> configs) {
        theSystemConfigMap = safeStream(configs)
                .collect(toConcurrentMap(SystemConfigDO::getId, Function.identity()));

    }

    @OnEvent
    public void handleSystemConfigsChange(SystemConfigCacheEvent event) {
        theSystemConfigMap.put(event.getDataObject().getId(), event.getDataObject());
    }

    @Override
    public void quiesceNode(int nodeId, boolean quiesce) {
        for (RouterServerChannel channel : theChannels) {
            if (channel.isRunning() && channel.getNodeId() == nodeId) {
                channel.sendQuiesceToggle(quiesce);
            }
        }
    }

    @Override
    public void quiesceAll(boolean quiesce) {
        for (RouterServerChannel channel : theChannels) {
            if (channel.isRunning()) {
                channel.sendQuiesceToggle(quiesce);
            }
        }
    }
}
