package gusl.router.config;

import gusl.core.tostring.ToString;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Internal Access to the Gateway from nodes.
 *
 * @author dhudson
 */
@Getter
@Setter
@NoArgsConstructor
public class GatewayAccess {

    private long accountId;
    private String accessToken;


    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
