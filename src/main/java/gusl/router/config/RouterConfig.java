package gusl.router.config;

import gusl.core.tostring.ToString;
import gusl.model.nodeconfig.NodeConfig;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@NoArgsConstructor
@Getter
@Setter
public class RouterConfig extends NodeConfig {
    private int registraPort;
    private GatewayAccess gatewayAccess;

    @Override
    public String toString() {
        return ToString.toString(this);
    }

}
