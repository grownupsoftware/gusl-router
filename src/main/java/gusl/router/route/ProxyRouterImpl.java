package gusl.router.route;

import gusl.core.utils.StringUtils;
import gusl.model.nodeconfig.NodeType;
import gusl.model.routerconfig.ResourceNode;
import gusl.node.transport.HttpRequestClientImpl;
import gusl.node.transport.HttpUtils;
import gusl.router.client.RouterHttpClient;
import gusl.router.errors.RouterErrors;
import gusl.router.register.RegistraService;
import lombok.CustomLog;
import org.jvnet.hk2.annotations.Service;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.Invocation;
import javax.ws.rs.client.InvocationCallback;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.core.Variant;
import java.io.DataInputStream;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Locale;
import java.util.Set;

import static gusl.node.transport.HttpUtils.*;

/**
 * @author dhudson
 */
@Service
@CustomLog
public class ProxyRouterImpl implements ProxyRouter {

    private static final String ROUTE_PATH = "route";

    @Inject
    private RouterHttpClient theRouterClient;

    @Inject
    private RegistraService registraService;

    private final Set<String> RESTRICTED_HEADERS = Set.of(
            "Access-Control-Request-Headers",
            "Access-Control-Request-Method",
            "Connection", /* close is allowed */
            "Content-Length",
            "Content-Transfer-Encoding",
            "Host",
            "Keep-Alive",
            "Origin",
            "Trailer",
            "Transfer-Encoding",
            "Upgrade",
            "Via"
    );

    public ProxyRouterImpl() {
    }

    @Override
    public void proxy(final AsyncResponse asyncResponse, UriInfo uriInfo, final HttpServletRequest servletRequest) {

        // Let's get the header to see where we are going.
        String nodeType = servletRequest.getHeader(NODE_ROUTE_HEADER_NAME);
        if (nodeType == null || nodeType.isEmpty()) {
            HttpUtils.resumeAsyncError(asyncResponse, Response.Status.BAD_REQUEST,
                    RouterErrors.NODE_ROUTE_NODE.getError());
            return;
        }

        NodeType node = NodeType.valueOf(nodeType);
        if (node == null) {
            HttpUtils.resumeAsyncError(asyncResponse, Response.Status.BAD_REQUEST,
                    RouterErrors.NODE_ROUTE_NODE.getError());
            return;
        }

        long balanceId = 0;
        String balId = servletRequest.getHeader(NODE_ROUTER_ID_HEADER_NAME);
        if (StringUtils.isNotBlank(balId)) {
            balanceId = Long.parseLong(balId);
        }

        ResourceNode cnode = registraService.getRoutedNodeFor(node, balanceId);

        if (cnode == null) {
            HttpUtils.resumeAsyncError(asyncResponse, Response.Status.BAD_REQUEST,
                    RouterErrors.NO_ROUTE_TO_NODE.getError(new String[]{node.name(), getPathInfo(uriInfo)}));
            return;
        }

        String forwardUrl = buildURL(cnode, uriInfo);

        // Let's build the new request
        Invocation.Builder builder =
                ((HttpRequestClientImpl) theRouterClient).getHttpClient().getClient().target(forwardUrl).request();

        Enumeration<String> headerNames = servletRequest.getHeaderNames();
        while (headerNames.hasMoreElements()) {
            String header = headerNames.nextElement();
            if (!RESTRICTED_HEADERS.contains(header)) {
                builder.header(header, servletRequest.getHeader(header));
            }
        }

        // Let's set the X-Forward-For
        builder.header(X_FORWARD_FOR_HEADER_NAME, servletRequest.getRemoteAddr());

        if (HttpUtils.isChunkedEncoding(servletRequest)) {
            logger.warn("Unable to proxy chunked coded requests with payloads {}", forwardUrl);
        }

        Entity<?> entity;
        if (HttpUtils.isChunkedEncoding(servletRequest) || servletRequest.getContentLength() > 0) {
            try {
                // Be nice to do this as a zero copy
                byte[] data = new byte[servletRequest.getContentLength()];
                DataInputStream dataIs = new DataInputStream(servletRequest.getInputStream());
                dataIs.readFully(data);

                // Keep compression encoding
                MediaType type = MediaType.valueOf(servletRequest.getContentType());
                String encoding = servletRequest.getHeader(CONTENT_ENCODING_HEADER_NAME);
                Variant variant = new Variant(type, (Locale) null, encoding);

                entity = Entity.entity(data, variant);
            } catch (IOException ex) {
                logger.warn("Unable to copy payload for {}", forwardUrl, ex);
                HttpUtils.resumeAsyncError(asyncResponse, RouterErrors.IO_EXCEPTION.getError(ex.getLocalizedMessage()));
                return;
            }
        } else {
            entity = Entity.entity("", servletRequest.getContentType());
        }

        builder.async().post(entity, new InvocationCallback<Response>() {
            @Override
            public void completed(Response response) {
                asyncResponse.resume(response);
            }

            @Override
            public void failed(Throwable throwable) {
                logger.warn("Proxy Request failed", throwable);
                HttpUtils.resumeAsyncError(asyncResponse, RouterErrors.REQUEST_FAILED.getError());
            }
        });
    }

    private String buildURL(ResourceNode node, UriInfo uriInfo) {
        StringBuilder builder = new StringBuilder(100);
        // Should be HTTP, but we never know
        builder.append(uriInfo.getAbsolutePath().getScheme());
        builder.append("://");
        builder.append(node.getHost());
        builder.append(":");
        builder.append(node.getPort());
        builder.append(getPathInfo(uriInfo));

        return builder.toString();
    }

    private String getPathInfo(UriInfo uriInfo) {
        StringBuilder builder = new StringBuilder(50);
        builder.append(uriInfo.getPath().substring(ROUTE_PATH.length()));

        String query = uriInfo.getRequestUri().getQuery();
        if (query != null) {
            builder.append("?");
            builder.append(query);
        }

        return builder.toString();
    }
}
