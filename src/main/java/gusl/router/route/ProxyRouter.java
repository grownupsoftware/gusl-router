package gusl.router.route;

import org.jvnet.hk2.annotations.Contract;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;

/**
 *
 * @author dhudson
 */
@Contract
public interface ProxyRouter {

    public void proxy(@Suspended final AsyncResponse asyncResponse, @Context UriInfo uriInfo, @Context final HttpServletRequest servletRequest);
}
