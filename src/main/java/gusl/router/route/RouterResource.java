package gusl.router.route;

import gusl.node.async.AsyncTimeoutConstants;
import gusl.node.providers.AsyncTimeout;
import gusl.node.resources.AbstractBaseResource;
import org.glassfish.jersey.server.ManagedAsync;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.AsyncResponse;
import javax.ws.rs.container.Suspended;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriInfo;
import java.net.URISyntaxException;

/**
 *
 * @author dhudson
 */
@Singleton
@Path(value = "/route/{seg: .*}")
public class RouterResource extends AbstractBaseResource {

    @Inject
    private ProxyRouter theProxyRouter;

    @POST
    // Allow for any routed media types
    @Produces(MediaType.WILDCARD)
    @Consumes(MediaType.WILDCARD)
    // Either side should timeout before this
    @AsyncTimeout(timeout = AsyncTimeoutConstants.MINUTES_30)
    public void routePost(@Suspended final AsyncResponse asyncResponse, @Context UriInfo uriInfo, @Context final HttpServletRequest servletRequest) throws URISyntaxException {
        theProxyRouter.proxy(asyncResponse, uriInfo, servletRequest);
    }

}
