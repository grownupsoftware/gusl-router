package gusl.router.status;

import gusl.node.resources.AbstractBaseResource;
import gusl.router.dto.NodesDTO;
import gusl.router.register.RegistraService;

import javax.inject.Inject;
import javax.inject.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * @author dhudson
 */
@Singleton
@Path("/status")
public class StatusResource extends AbstractBaseResource {

    @Inject
    private RegistraService theRegistra;

    @GET
    public NodesDTO getNodes() {
        NodesDTO response = new NodesDTO();
        response.setNodes(theRegistra.getNodes());
        return response;
    }

}
