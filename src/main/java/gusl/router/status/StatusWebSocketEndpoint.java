package gusl.router.status;

import gusl.core.eventbus.OnEvent;
import gusl.model.status.NodeChangedMessage;
import gusl.node.bootstrap.GUSLServiceLocator;
import gusl.node.eventbus.NodeEventBus;
import gusl.node.websockets.AbstractRequestAwareWebsocket;

import javax.websocket.*;
import javax.websocket.server.ServerEndpoint;

/**
 * Status change listener for Web Sockets.
 * <p>
 * Remember there is one of these constructed for each web socket client.
 *
 * @author dhudson
 */
@ServerEndpoint(value = "/wsnodes")
public class StatusWebSocketEndpoint extends AbstractRequestAwareWebsocket {

    private final NodeEventBus theEventBus;

    public StatusWebSocketEndpoint() {
        theEventBus = GUSLServiceLocator.getService(NodeEventBus.class);
    }

    @Override
    @OnOpen
    public void onOpen(Session session, EndpointConfig config) {
        super.onOpen(session, config);
        theEventBus.register(this);
    }

    @Override
    @OnClose
    public void onClose(Session session, CloseReason reason) {
        super.onClose(session, reason);
        theEventBus.unregister(this);
    }

    @Override
    @OnError
    public void onError(Session session, Throwable t) {
        super.onError(session, t);
        theEventBus.unregister(this);
    }

    @OnEvent
    public void nodeMessage(NodeChangedMessage message) {
        writeJSON(message);
    }
}
