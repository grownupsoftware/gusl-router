package gusl.router.events;

import gusl.model.routerconfig.ResourceNode;

import java.util.List;

/**
 *
 * @author dhudson
 */
public class NodesChangedEvent extends AbstractNodeEvent {

    public NodesChangedEvent() {
    }

    public NodesChangedEvent(List<ResourceNode> nodes) {
        super(nodes);
    }
}
