package gusl.router.events;

/**
 * Fired when a node has crashed.
 *
 * @author dhudson
 */
public class NodeCrashedEvent {

    private int nodeId;

    public NodeCrashedEvent() {

    }

    public NodeCrashedEvent(int id) {
        nodeId = id;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

}
