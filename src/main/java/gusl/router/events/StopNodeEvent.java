package gusl.router.events;

import gusl.model.event.AbstractEvent;

/**
 * This is fired when a user as requested that a node is stopped from the UI.
 *
 * @author dhudson
 */
public class StopNodeEvent extends AbstractEvent {

    private int nodeId;

    public StopNodeEvent() {
    }

    public StopNodeEvent(int id) {
        nodeId = id;
    }

    public int getNodeId() {
        return nodeId;
    }

    public void setNodeId(int nodeId) {
        this.nodeId = nodeId;
    }

}
