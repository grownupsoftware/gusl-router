package gusl.router.events;

import gusl.model.routerconfig.ResourceNode;

import java.util.List;

/**
 *
 * @author dhudson
 */
public class NodeRegisteredEvent extends AbstractNodeEvent {

    public NodeRegisteredEvent() {
    }

    public NodeRegisteredEvent(List<ResourceNode> nodes) {
        super(nodes);
    }
}
