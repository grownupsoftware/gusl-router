package gusl.router.events;

import gusl.core.tostring.ToString;
import gusl.model.routerconfig.ResourceNode;

import java.util.List;

/**
 * @author dhudson
 */
public class NodeStartedEvent extends AbstractNodeEvent {

    private ResourceNode startedNode;

    public NodeStartedEvent() {
    }

    public NodeStartedEvent(List<ResourceNode> nodes, ResourceNode node) {
        super(nodes);
        startedNode = node;
    }

    public ResourceNode getStartedNode() {
        return startedNode;
    }

    public void setStartedNode(ResourceNode startedNode) {
        this.startedNode = startedNode;
    }

    @Override
    public String toString() {
        return ToString.toString(this);
    }
}
