package gusl.router.events;

import gusl.model.routerconfig.ResourceNode;
import gusl.router.model.ApplicationNodeHelper;

import java.util.List;

/**
 *
 * @author dhudson
 */
public abstract class AbstractNodeEvent {

    private List<ResourceNode> theNodes;

    public AbstractNodeEvent() {
    }

    public AbstractNodeEvent(List<ResourceNode> nodes) {
        theNodes = nodes;
    }

    public List<ResourceNode> getNodes() {
        return theNodes;
    }

    public void setNodes(List<ResourceNode> nodes) {
        theNodes = nodes;
    }

    public List<ResourceNode> getActiveNodes() {
        return ApplicationNodeHelper.getActiveNodes(theNodes);
    }
}
