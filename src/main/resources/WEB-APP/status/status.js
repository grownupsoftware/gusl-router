var app = angular.module('sortApp', [])

    .controller('mainController', function ($scope, $http, $timeout, $window) {
        $scope.sortType = 'type'; // set the default sort type
        $scope.sortReverse = false;  // set the default sort order
        $scope.searchValue = '';     // set the default search/filter term

        $scope.nodeCount = 0;
        $scope.activeCount = 0;
        $scope.crashedCount = 0;

        $scope.constructType = '';
        $scope.constructName = '';
        $scope.constructColour = ''; // Construct colour

        $scope.wsImage = "redButton";

        $scope.wsUpdate = function () {
            $scope.wsImage = "yellowButton";
            $timeout(function () {
                $scope.wsImage = "greenButton";
            }, 500);
            $scope.recount();
            $scope.$apply();
        };

        $scope.quiesceNode = function (nodeRecord) {
            $http({
                method: 'POST',
                url: '/router/rest/registra/quiesce/' + nodeRecord.nodeId
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.deregisterNode = function (nodeRecord) {
            $http({
                method: 'POST',
                url: '/router/rest/registra/deregister/' + nodeRecord.nodeId
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.stopNode = function (nodeRecord) {
            $http({
                method: 'POST',
                url: '/router/rest/registra/stop/' + nodeRecord.nodeId
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.resetRegistrationCache = function () {
            $http({
                method: 'POST',
                url: '/router/rest/publish/event/?class=lm.model.event.ResetRegistrationCacheEvent'
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.dumpCache = function () {
            $http({
                method: 'POST',
                url: '/router/rest/publish/event/?class=lm.model.event.DumpCacheEvent'
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.logCache = function () {
            $http({
                method: 'POST',
                url: '/router/rest/publish/event/?class=lm.model.event.LogCacheEvent'
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.splash = function () {
            $http({
                method: 'POST',
                url: '/router/rest/publish/event/?class=lm.node.events.SplashEvent'
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.quiesce = function () {
            $http({
                method: 'POST',
                url: '/router/rest/registra/quiesceAll'
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.nodeDetails = function (nodeRecord) {
            var tabWindowId = $window.open("about:blank", "_blank");
            $http({
                method: 'POST',
                url: nodeRecord.applicationUrl + '/details'
            }).then(function successCallback(response) {
                tabWindowId.document.write('<pre>' + JSON.stringify(response.data, null, 2) + '</pre>');
            }, function errorCallback(response) {
            });
        };

        $scope.recount = function () {
            $scope.nodeCount = 0;
            $scope.activeCount = 0;
            $scope.crashedCount = 0;

            for (var j = 0; j < $scope.nodeConfigs.length; j++) {
                var config = $scope.nodeConfigs[j];
                $scope.nodeCount++;
                if (config.status === 'ACTIVE') {
                    $scope.activeCount++;
                }
                if (config.status === 'CRASHED') {
                    $scope.crashedCount++;
                }
            }
        };

        $http({
            method: 'GET',
            url: getContextPath() + '/rest/info'
        }).then(function successCallback(response) {
            //console.log('response', response);
            var details = response.data.nodeDetails;
            $scope.constructType = details.operationalMode + '-' + details.instanceType;
            $scope.constructName = details.constructName;
            $scope.constructColour = constructToColour($scope.constructName);
        });

        $http({
            method: 'GET',
            url: getContextPath() + '/rest/status'
        }).then(function successCallback(response) {
            console.log('response', response);
            var nodeConfigs = [];
            response.data.nodes.forEach(function (nodeConfig) {
                nodeConfigs.push(nodeConfig);
            });
            $scope.nodeConfigs = nodeConfigs;
            $scope.recount();
        }, function errorCallback(response) {
        });

        function startWebSockets() {
            console.log('starting...');

            // establish the communication channel over a websocket
            var ws = new WebSocket("ws://" + window.location.host + getContextPath() + "/wsnodes");

            // called when socket connection established
            ws.onopen = function () {
                console.log("Connected to Router Node Listener!");
                $scope.wsImage = "greenButton";
                $scope.$apply();
            };

            // called when a message received from server
            ws.onmessage = function (evt) {
                var changeMessage = JSON.parse(evt.data);
                var data = changeMessage.node;
                var id = data.nodeId;

                if (changeMessage.type === 'STATUS_CHANGE' || changeMessage.type === 'REGISTER') {
                    //Lets see if it already exists
                    var index;
                    var config;
                    for (index = 0; index < $scope.nodeConfigs.length; index++) {
                        config = $scope.nodeConfigs[index];
                        if (config.nodeId === id) {
                            $scope.nodeConfigs[index] = data;
                            $scope.wsUpdate();
                            return;
                        }
                    }

                    // Its new
                    $scope.nodeConfigs.push(data);
                }

                if (changeMessage.type === 'DEREGISTER') {
                    var index;
                    var config;
                    for (index = 0; index < $scope.nodeConfigs.length; index++) {
                        config = $scope.nodeConfigs[index];
                        if (config.nodeId === id) {
                            $scope.nodeConfigs.splice(index, 1);
                        }
                    }
                }

                $scope.wsUpdate();
            };

            // called when socket connection closed
            ws.onclose = function () {
                console.log("Disconnected from router");
                $scope.wsImage = "redButton";
                $scope.$apply();
            };

            // called in case of an error
            ws.onerror = function (err) {
                console.log("ERROR!", err);
            };

            // sends msg to the server over websocket
            function sendToServer(msg) {
                ws.send(msg);
            }
        }

        function getContextPath() {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
        }

        function constructToColour(constructName) {
            if (constructName) {
                var i, l, hval = 0x811c9dc5;
                for (i = 0, l = constructName.length; i < l; i++) {
                    hval ^= constructName.charCodeAt(i);
                    hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
                }
                return '#' + ("00000" + (hval >>> 0).toString(16)).substr(-6);
            }
            return '#00000';
        }

        startWebSockets();

    });

app.directive('metricsLink', function () {
    return {
        restrict: 'A',
        scope: {
            row: '='
        },
        replace: false, // Replace with the template below
        transclude: false, // we want to insert custom content inside the directive
        link: function (scope, element, attrs) {
            element.attr("target", "_blank");
            var link = scope.row.applicationUrl + '/metrics/index.html';
            element.attr("href", link);
        }
    };
});

app.directive('popOver', function ($http, $window) {
    return {
        restrict: 'C',
        link: function (scope, element, attr) {
            var href = attr.href;

            element.tooltip();
            element.bind('mouseover', function (e) {

                $http({
                    method: 'POST',
                    url: href
                }).success(function (data) {
                    var tabWindowId = $window.open("about:blank", "_blank");
                    tabWindowId.location.href = "";
//                    attr.$set('originalTitle', JSON.stringify(data, null, 2));
//                    element.tooltip('show');
                    tabWindowId.document.body.innerHTM = JSON.stringify(data, null, 2);
                });
            });
        }
    };
});

app.directive('goClick', function ($window) {
    return function (scope, element, attrs) {
        var path;

        attrs.$observe('goClick', function (val) {
            path = val;
        });

        element.bind('click', function () {
            scope.$apply(function () {
                //$location.replace();
                //$location.path( path );
                $window.location.href = path;
            });
        });
    };
});

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
}
