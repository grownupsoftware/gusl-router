var app = angular.module('sortApp', [])

    .controller('mainController', function ($scope, $http) {

        $scope.constructType = '';
        $scope.constructName = '';
        $scope.constructColour = ''; // Construct colour

        $scope.dateChanged = '';
        $scope.systemConfigs = '';
        $scope.uuid = '';

        $scope.reloadSystemProperties = function () {
            $http({
                method: 'POST',
                url: '/router/rest/system/reloadSystemConfig'
            }).then(function successCallback(response) {
            }, function errorCallback(response) {
            });
        };

        $scope.generateUUID = function () {
            $http({
                method: 'POST',
                url: '/router/rest/uuid/generate'
            }).then(function successCallback(response) {
                $scope.uuid = response.data['uuid'];
            }, function errorCallback(response) {
            });
        };

        $http({
            method: 'GET',
            url: getContextPath() + '/rest/info'
        }).then(function successCallback(response) {
            //console.log('response', response);
            let details = response.data['node-details'];
            $scope.constructType = details['operational-mode'] + '-' + details['instance-type'];
            $scope.constructName = details['construct-name'];
            $scope.constructColour = constructToColour($scope.constructName);
        });

        $http({
            method: 'POST',
            url: getContextPath() + '/rest/system/getSystemDetails'
        }).then(function successCallback(response) {
            console.log('response', response);
            let systemDetails = response.data;
            $scope.dateChanged = systemDetails['date-changed']
            $scope.systemConfigs = JSON.stringify(systemDetails['configs'], null, 2)

        }, function errorCallback(response) {
        });


        $http({
            method: 'POST',
            url: getContextPath() + '/rest/uuid'
        }).then(function successCallback(response) {
            console.log('response', response);
            $scope.uuid = response.data['uuid'];
        });

        function getContextPath() {
            return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
        }

        function constructToColour(constructName) {
            if (constructName) {
                let i, l, hval = 0x811c9dc5;
                for (i = 0, l = constructName.length; i < l; i++) {
                    hval ^= constructName.charCodeAt(i);
                    hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
                }
                return '#' + ("00000" + (hval >>> 0).toString(16)).substr(-6);
            }
            return '#00000';
        }

    });

app.directive('goClick', function ($window) {
    return function (scope, element, attrs) {
        var path;

        attrs.$observe('goClick', function (val) {
            path = val;
        });

        element.bind('click', function () {
            scope.$apply(function () {
                //$location.replace();
                //$location.path( path );
                $window.location.href = path;
            });
        });
    };
});

function getContextPath() {
    return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
}
