
'use strict';

// https://developers.google.com/chart/interactive/docs/reference#dataparam
// DH. Change to wrapper object

google.charts.load('current', {'packages': ['gauge', 'corechart']});

google.charts.setOnLoadCallback(function () {
    angular.bootstrap(document.body, ['sortApp']);
});

var app = angular.module('sortApp', [])

        .controller('mainController', function ($scope, $http, $timeout) {
            $scope.data = google.visualization.arrayToDataTable([
                ['Label', 'Value'],
                ['CPU', 0]
            ]);

            $scope.options = {
                width: 400, height: 120,
                redFrom: 90, redTo: 100,
                yellowFrom: 75, yellowTo: 90,
                minorTicks: 5
            };

            $scope.chart = new google.visualization.Gauge(document.getElementById('chart_div'));
            $scope.chart.draw($scope.data, $scope.options);

            $scope.memChart = new google.visualization.AreaChart(document.getElementById('memory_div'));
            $scope.memOptions = {
                title: 'Memory Consumption',
                hAxis: {title: 'Time', titleTextStyle: {color: '#333'}},
                vAxis: {minValue: 0}
            };
            $scope.memData = google.visualization.arrayToDataTable([
                ['Time', 'Used', 'Max'],
                [new Date(), 0, 0]
            ]);

            $scope.wsImage = "redButton";

            $scope.wsUpdate = function () {
                $scope.wsImage = "yellowButton";
                $timeout(function () {
                    $scope.wsImage = "greenButton";
                }, 500);
                $scope.$apply();
            };

            $http({
                method: 'GET',
                url: '/router/rest/status'
            }).then(function successCallback(response) {
                console.log('response', response);
                var nodeConfigs = [];
                response.data.nodes.forEach(function (nodeConfig) {
                    nodeConfigs.push(nodeConfig);
                });
                $scope.nodeConfigs = nodeConfigs;
            }, function errorCallback(response) {
            });

            function startWebSockets() {
                console.log('starting...');

                // establish the communication channel over a websocket
                var ws = new WebSocket("ws://" + window.location.host + getContextPath() + "/wsmonitor");

                // called when socket connection established
                ws.onopen = function () {
                    console.log("Connected to Router Node Listener!");
                    $scope.wsImage = "greenButton";
                    $scope.$apply();
                };

                // called when a message received from server
                ws.onmessage = function (evt) {
                    var message = JSON.parse(evt.data);
                    console.log("MonitorMessage " + evt.data);
                    var nodeMetrics = message['metrics'];
                    var load = nodeMetrics['system-load'];
                    // Convert to some sort of %
                    load = load * 10;
                    $scope.data.setValue(0, 1, load);
                    $scope.chart.draw($scope.data, $scope.options);

                    $scope.memData.addRow([new Date(), nodeMetrics['used-memory'], nodeMetrics['max-memory']]);
                    $scope.memChart.draw($scope.memData, $scope.memOptions);

                    $scope.wsUpdate();
                };

                // called when socket connection closed
                ws.onclose = function () {
                    console.log("Disconnected from router");
                    $scope.wsImage = "redButton";
                    $scope.$apply();
                };

                // called in case of an error
                ws.onerror = function (err) {
                    console.log("ERROR!", err);
                };

                // sends msg to the server over websocket
                function sendToServer(msg) {
                    ws.send(msg);
                }
            }

            function getContextPath() {
                return window.location.pathname.substring(0, window.location.pathname.indexOf("/", 2));
            }

            startWebSockets();

        });
